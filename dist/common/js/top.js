// JavaScript Document
$(function(){
	$(".togleContent").hide();
	var flag = "閉じる";
	$(".togleBtn").on("click", function(e) {
		e.preventDefault(); //aタグのリンク無効化
		$(this).prev(".togleContent").slideToggle();

        var i = $(this).index(this)
        var p = $(this).prev(".togleContent").eq(i).offset().top;
        $('html,body').animate({ scrollTop: p }, '300');
        
		
		if(flag == "閉じる"){ //もしflagがcloseだったら
			$(this).text("閉じる");
			flag = "イベント情報を見る"; //flagをopenにする
		}
		else{ //もしflagがopenだったら
			$(this).text("イベント情報を見る");
			flag = "閉じる"; //flagをcloseにする
		}

		return false;
		
	});
	$(".my-tooltip").tooltip({
		direction: "top",
		duration: 300
	});
});