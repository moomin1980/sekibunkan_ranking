# coding:utf-8

import os
import sys
import datetime

dt_now   = datetime.datetime.now()
year  = str( dt_now.year )
month = str( '{0:02d}'.format( dt_now.month ) )
day   = str( '{0:02d}'.format( dt_now.day ) )


# ---- Function
def gen_html( csv, filename, template, book ):

    cds  = []
    cda  = []
    dvd  = []
    game = []

    for row in csv:
        # print( row )
        category = row[0]
        rank = row[1]
        title = row[2]
        author = hw = row[3]
        # tsutaya_url = row[4]
        jan = row[7]
        maker = row[8]
        if( 9 <= len(row) ):
            honya_url = row[9]
        else :
            honya_url = ""
        if( 11 <= len(row) ):
            trans = row[11]
        else:
            trans = ""
        # print( category + " " + jan + " " + honya_url )

        if( category == "cds" ):
            cds.append( [rank, title, author, honya_url, trans] )
        elif( category == "cda" ):
            cda.append( [rank, title, author, honya_url, trans] )
        elif( category == "dvd" ):
            dvd.append( [rank, title, author, honya_url, trans] )
        elif( category == "game" ):
            game.append( [rank, title, hw, maker, trans] )

    # print( cds )
    # print( cda )
    # print( dvd )
    # print( game )

    # Read book template file
    with open(book, encoding="utf_8_sig") as f:
        book = f.readlines()

    # Read template file
    with open(template, encoding="utf_8_sig") as f:
        reader = f.readlines()

    header = ""
    footer = ""
    flag = True
    for row in reader:
        if( row.strip() == "<!-- __REPLACE_AREA__ -->" ):
            flag = False
        elif( flag ):
            header += row
        else:
            footer += row

    # Generate HTML code
    f = open( filename, 'w', encoding="utf_8_sig")
    # Write html header
    f.write( header )

    # Book part
    for i in book:
        code = i
        f.write( code )

    # CD Single part
    code = cds_part( cds )
    f.write( code )
    # CD Album part
    code = cda_part( cda )
    f.write( code )
    # DVD part
    code = dvd_part( dvd )
    f.write( code )
    # GAME part
    code = game_part( game )
    f.write( code )

    # Wrtie html footer
    f.write( footer )
    f.close()


# ---- CD Single part
def cds_part( data ):
    pre = \
        '      <section class="sec_cds">\n'\
        '        <a name="cds" id="cds"></a>\n'\
        '        <h2><img src="assets/cds_title.jpg" alt="CDシングルランキングBEST10" class="mtb-15" /></h2>\n'\
        '        <!--CDシングル-->\n'\
        '        <div id="cds_area" class="ranking_detail">\n'\
        '          <div class=update_caption>\n'\
        '            <span>タイトルおよび詳細ボタンをクリックすると詳細をご覧になれます。</span>\n'\
        '            <span>※' + year + '年' + month + '月' + day + '日更新</span>\n'\
        '          </div>\n'\
        '          <!--CDシングルランキング-->\n'\
        '          <table>\n'\
        '            <tr class="head">\n'\
        '              <th>順位</th>\n'\
        '              <th>推移</th>\n'\
        '              <th></th>\n'\
        '              <th>アーティスト</th>\n'\
        '              <th>タイトル</th>\n'\
        '              <th>詳細</th>\n'\
        '            </tr>\n'\

    main = ""
    for list in data:
        rank = list[0]
        title = list[1]
        author = list[2]
        honya_url = list[3]
        trans = list[4]
        if( trans ):
            trans_class = trans
            if( trans == "new"):
                trans_text  = "NEW"
            elif( trans == "up" ):
                trans_text  = "↑"
            elif( trans == "stay" ):
                trans_text  = "→"
            else:
                trans_text  = "↓"
        else:
            trans_class = "new"
            tran_text   = "NEW"

        if( int(rank) % 2 ):
            class_name = "odd"
        else:
            class_name = "even"

        if( honya_url ):
            main += '            <tr class="' + class_name + '">\n'
            main += '              <td><img src="assets/' + rank + '.gif" alt="CDシングルランキング第' + rank + '位" /></td>\n'
            main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank"><img src="images/cds/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="CDランキング第' + rank + '位" /></a></td>\n'
            main += '              <td>' + author + '</td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank">' + title + '</a></td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank"><img src="assets/more.jpg" alt="このCDの詳細を見る" /></a></td>\n'
            main += '            </tr>\n'
        else:
            main += '            <tr class="' + class_name + '">\n'
            main += '              <td><img src="assets/' + rank + '.gif" alt="CDシングルランキング第' + rank + '位" /></td>\n'
            main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
            main += '              <td><img src="images/cds/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="CDランキング第' + rank + '位" /></td>\n'
            main += '              <td>' + author + '</td>\n'
            main += '              <td>' + title + '</td>\n'
            main += '              <td><img src="assets/more.jpg" alt="このCDの詳細を見る" /></td>\n'
            main += '            </tr>\n'


    post = \
        '          </table>\n'\
        '          <br />\n'\
        '        </div>\n'\
        '      </section>\n'\
        '      \n'

    # Output
    return pre + main + post

# ---- CD Album part
def cda_part( data ):
    pre = \
        '      <section class="sec_cda">\n'\
        '        <a name="cda" id="cda"></a>\n'\
        '        <h2><img src="assets/cda_title.jpg" alt="CDアルバムランキングBEST10" class="mtb-15" /></h2>\n'\
        '        <!--CDアルバム-->\n'\
        '        <div id="cda_area" class="ranking_detail">\n'\
        '          <div class=update_caption>\n'\
        '            <span>タイトルおよび詳細ボタンをクリックすると詳細をご覧になれます。</span>\n'\
        '            <span>※' + year + '年' + month + '月' + day + '日更新</span>\n'\
        '          </div>\n'\
        '          <!--CDアルバムランキング-->\n'\
        '          <table>\n'\
        '            <tr class="head">\n'\
        '              <th>順位</th>\n'\
        '              <th>推移</th>\n'\
        '              <th></th>\n'\
        '              <th>アーティスト</th>\n'\
        '              <th>タイトル</th>\n'\
        '              <th>詳細</th>\n'\
        '            </tr>\n'\

    main = ""
    for list in data:
        rank = list[0]
        title = list[1]
        author = list[2]
        honya_url = list[3]
        trans = list[4]
        if( trans ):
            trans_class = trans
            if( trans == "new"):
                trans_text  = "NEW"
            elif( trans == "up" ):
                trans_text  = "↑"
            elif( trans == "stay" ):
                trans_text  = "→"
            else:
                trans_text  = "↓"
        else:
            trans_class = "new"
            tran_text   = "NEW"

        if( int(rank) % 2 ):
            class_name = "odd"
        else:
            class_name = "even"

        if( honya_url ):
            main += '            <tr class="' + class_name + '">\n'
            main += '              <td><img src="assets/' + rank + '.gif" alt="CDアルバムランキング第' + rank + '位" /></td>\n'
            main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank"><img src="images/cda/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="CDランキング第' + rank + '位" /></a></td>\n'
            main += '              <td>' + author + '</td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank">' + title + '</a></td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank"><img src="assets/more.jpg" alt="このCDの詳細を見る" /></a></td>\n'
            main += '            </tr>\n'
        else:
            main += '            <tr class="' + class_name + '">\n'
            main += '              <td><img src="assets/' + rank + '.gif" alt="CDアルバムランキング第' + rank + '位" /></td>\n'
            main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
            main += '              <td><img src="images/cda/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="CDランキング第' + rank + '位" /></td>\n'
            main += '              <td>' + author + '</td>\n'
            main += '              <td>' + title + '</td>\n'
            main += '              <td><img src="assets/more.jpg" alt="このCDの詳細を見る" /></td>\n'
            main += '            </tr>\n'

    post = \
        '          </table>\n'\
        '          <br />\n'\
        '        </div>\n'\
        '      </section>\n'\
        '      \n'

    # Output
    return pre + main + post

# ---- DVD part
def dvd_part( data ):
    pre = \
        '      <section class="sec_dvd">\n'\
        '        <a name="dvd" id="dvd"></a>\n'\
        '        <h2><img src="assets/dvd_title.jpg" alt="DVDランキングBEST10" class="mtb-15" /></h2>\n'\
        '        <!--DVD-->\n'\
        '        <div id="dvd_area" class="ranking_detail">\n'\
        '          <div class=update_caption>\n'\
        '            <span>タイトルおよび詳細ボタンをクリックすると詳細をご覧になれます。</span>\n'\
        '            <span>※' + year + '年' + month + '月' + day + '日更新</span>\n'\
        '          </div>\n'\
        '          <!--DVDランキング-->\n'\
        '          <table>\n'\
        '            <tr class="head">\n'\
        '              <th>順位</th>\n'\
        '              <th>推移</th>\n'\
        '              <th></th>\n'\
        '              <th>アーティスト・主演</th>\n'\
        '              <th>タイトル</th>\n'\
        '              <th>詳細</th>\n'\
        '            </tr>\n'\

    main = ""
    for list in data:
        rank = list[0]
        title = list[1]
        author = list[2]
        honya_url = list[3]
        trans = list[4]
        if( trans ):
            trans_class = trans
            if( trans == "new"):
                trans_text  = "NEW"
            elif( trans == "up" ):
                trans_text  = "↑"
            elif( trans == "stay" ):
                trans_text  = "→"
            else:
                trans_text  = "↓"
        else:
            trans_class = "new"
            tran_text   = "NEW"

        if( int(rank) % 2 ):
            class_name = "odd"
        else:
            class_name = "even"

        if( honya_url ):
            main += '            <tr class="' + class_name + '">\n'
            main += '              <td><img src="assets/' + rank + '.gif" alt="DVDランキング第' + rank + '位" /></td>\n'
            main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank"><img src="images/dvd/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="DVDランキング第' + rank + '位" /></a></td>\n'
            main += '              <td>' + author + '</td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank">' + title + '</a></td>\n'
            main += '              <td><a href="' + honya_url + '" target="_blank"><img src="assets/more.jpg" alt="このDVDの詳細を見る" /></a></td>\n'
            main += '            </tr>\n'
        else:
            main += '            <tr class="' + class_name + '">\n'
            main += '              <td><img src="assets/' + rank + '.gif" alt="DVDランキング第' + rank + '位" /></td>\n'
            main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
            main += '              <td><img src="images/dvd/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="DVDランキング第' + rank + '位" /></td>\n'
            main += '              <td>' + author + '</td>\n'
            main += '              <td>' + title + '</td>\n'
            main += '              <td><img src="assets/more.jpg" alt="このDVDの詳細を見る" /></td>\n'
            main += '            </tr>\n'

    post = \
        '          </table>\n'\
        '          <br />\n'\
        '        </div>\n'\
        '      </section>\n'\
        '      \n'

    # Output
    return pre + main + post


# ---- GAME part
def game_part( data ):
    pre = \
        '      <section class="sec_game">\n'\
        '        <a name="game" id="game"></a>\n'\
        '        <h2><img src="assets/game_title.jpg" alt="ゲームソフトランキングBEST10" width="750" height="40" class="mtb-15" /></h2>\n'\
        '        <!--GAME-->\n'\
        '        <div id="game_area" class="ranking_detail">\n'\
        '          <div class=update_caption>\n'\
        '            <span>タイトルおよび詳細ボタンをクリックすると詳細をご覧になれます。</span>\n'\
        '            <span>※' + year + '年' + month + '月' + day + '日更新</span>\n'\
        '          </div>\n'\
        '          <!--GAMEランキング-->\n'\
        '          <table>\n'\
        '            <tr class="head">\n'\
        '              <th>順位</th>\n'\
        '              <th>推移</th>\n'\
        '              <th></th>\n'\
        '              <th>ハード</th>\n'\
        '              <th>タイトル</th>\n'\
        '              <th>メーカー</th>\n'\
        '            </tr>\n'\

    main = ""
    for list in data:
        rank = list[0]
        title = list[1]
        hw = list[2]
        maker = list[3]
        trans = list[4]
        if( trans ):
            trans_class = trans
            if( trans == "new"):
                trans_text  = "NEW"
            elif( trans == "up" ):
                trans_text  = "↑"
            elif( trans == "stay" ):
                trans_text  = "→"
            else:
                trans_text  = "↓"
        else:
            trans_class = "new"
            tran_text   = "NEW"

        if( int(rank) % 2 ):
            class_name = "odd"
        else:
            class_name = "even"

        main += '            <tr class="' + class_name + '">\n'
        main += '              <td><img src="assets/' + rank + '.gif" alt="ゲームソフトランキング第' + rank + '位" /></td>\n'
        main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
        main += '              <td><img src="images/game/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="GAMEランキング第' + rank + '位" /></a></td>\n'
        main += '              <td>' + hw + '</td>\n'
        main += '              <td>' + title + '</td>\n'
        main += '              <td>' + maker + '</a></td>\n'
        main += '            </tr>\n'

    post = \
        '            </table>\n'\
        '            <br />\n'\
        '          </div>\n'\
        '        </section>\n'\
        '        \n'

    # Output
    return pre + main + post

# ---- Book part
def book_part( data ):

    pre = \
        '      <section class="sec_book">\n'\
        '        <a name="books" id="books"></a>\n'\
        '        <!--book-->\n'\
        '        <h2><img src="assets/book_title.jpg" alt="本・書籍ランキングBEST10" class="mtb-15" /></h2>\n'\
        '        <div id="book_area" class="ranking_detail">\n'\
        '          <div class=update_caption>\n'\
        '            <span>タイトルおよび詳細ボタンをクリックすると詳細をご覧になれます。</span>\n'\
        '            <span>※' + year + '年' + month + '月' + day + '日更新</span>\n'\
        '          </div>\n'\
        '          <!--book ranking-->\n'\
        '          <table>\n'\
        '            <tr class="head">\n'\
        '              <th>順位</th>\n'\
        '              <th>推移</th>\n'\
        '              <th></th>\n'\
        '              <th>タイトル</th>\n'\
        '              <th>著者</th>\n'\
        '              <th>出版社</th>\n'\
        '              <th>詳細</th>\n'\
        '            </tr>\n'\


    main = ""
    for list in data:
        rank = list[0]
        title = list[1]
        author = list[2]
        publisher = list[3]
        honya_url = list[4]
        trans = list[5]
        if( trans ):
            trans_class = trans
            if( trans == "new"):
                trans_text  = "NEW"
            elif( trans == "up" ):
                trans_text  = "↑"
            elif( trans == "stay" ):
                trans_text  = "→"
            else:
                trans_text  = "↓"
        else:
            trans_class = "new"
            tran_text   = "NEW"

        if( int(rank) % 2 ):
            class_name = "odd"
        else:
            class_name = "even"

        main += '            <tr class="' + class_name + '">\n'
        main += '              <td><img src="assets/' + rank + '.gif" alt="本・書籍ランキング第' + rank + '位" /></td>\n'
        main += '              <td class="' + trans_class + '"><font><b>' +  trans_text + '</b></font></td>\n'
        main += '              <td><a href="' + honya_url + '" target="_blank"><img src="images/book/' + '{0:02d}'.format(int(rank)) + '.jpg" alt="書籍ランキング第' + rank + '位" /></a></td>\n'
        main += '              <td><a href="' + honya_url + '" target="_blank">' + title + '</a></td>\n'
        main += '              <td>' + author + '</td>\n'
        main += '              <td class="f10">' + publisher + '</td>\n'
        main += '              <td><a href="' + honya_url + '" target="_blank"><img src="assets/more.jpg" alt="この本の詳細を見る" /></a></td>\n'
        main += '            </tr>\n'

    post = \
        '          </table>\n'\
        '          <br />\n'\
        '        </div>\n'\
        '      </section>\n'\
        '      \n'

    # Output
    return pre + main + post

# ---- Main
if __name__ == '__main__':
    args = sys.argv

    in_csv   = args[1]
    out_html = args[2]
    in_book_html = args[3]

    gen_html( in_csv, out_html, in_book_html )
