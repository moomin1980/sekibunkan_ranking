# coding:utf-8

import os
import sys
import urllib3

# url = "https://store-tsutaya.tsite.jp/images/cache/tmb/d5bb1c4adea4aeacabc0aca96bb71a4a.jpg"
# filename = "sample.jpg"

# ---- Function
def img_dl( url, filename ):

    http = urllib3.PoolManager()
    r = http.request('GET', url)
    # print( r.data )
    image = r.data

    dir = os.path.dirname( filename )
    if not os.path.isdir( dir ):
        os.makedirs( dir )

    with open(filename, "wb") as a:
        a.write(image)

    print( "    [Save] : " + url )
    print( "             -> " + filename )

# ---- Main
if __name__ == '__main__':
    args = sys.argv

    if len( args ) < 3:
        print( "error")
    else:
        url = args[1]
        filename = args[2]
        img_dl( url, filename )
