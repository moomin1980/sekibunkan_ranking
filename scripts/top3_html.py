# coding:utf-8

import os
import sys
import datetime
import csv

dt_now   = datetime.datetime.now()
year  = str( dt_now.year )
month = str( '{0:02d}'.format( dt_now.month ) )
day   = str( '{0:02d}'.format( dt_now.day ) )


# ---- Function
def gen_html( tsutaya_csv, sekibunkan_csv, output_html ):

    book = []
    cds  = []
    cda  = []
    dvd  = []
    game = []

    for row in tsutaya_csv:
        # print( row )
        category = row[0]
        rank     = row[1]
        title    = row[2]
        author   = hw = row[3]
        maker    = row[8]
        if( 9 < len(row) ):
            honya_url = row[9]
        else :
            honya_url = ""
        if( 10 < len(row) ):
            price = row[10]
        else:
            price = ""
        # print( category + " " + jan + " " + honya_url )

        if( int(rank) <= 3 ):
            if( category == "cds" ):
                cds.append( [rank, title, author, honya_url, price] )
            elif( category == "cda" ):
                cda.append( [rank, title, author, honya_url, price] )
            elif( category == "dvd" ):
                dvd.append( [rank, title, author, honya_url, price] )
            elif( category == "game" ):
                game.append( [rank, title, hw, maker, price] )

    for row in sekibunkan_csv:
        # print( row )
        category  = row[0]
        rank      = row[1]
        title     = row[2]
        author    = row[3]
        price     = row[5]
        honya_url = row[7]

        if( int(rank) <= 3 ):
            if( category == "book" ):
                book.append( [rank, title, author, honya_url, price] )

    # print( book )
    # print( cds )
    # print( cda )
    # print( dvd )
    # print( game )

    code  = category_block( book, "book" )
    code += category_block( cds, "cds" )
    code += category_block( cda, "cda" )
    code += category_block( dvd, "dvd" )
    code += category_block( game, "game" )

    # print( code )

    # Generate HTML code
    f = open( output_html, 'w', encoding="utf_8_sig")
    # Write html header
    f.write( code )



# ----

def category_block( ranking_list, category ):
    if( category == "book" ):
        alink = "books"
        text1 = "積文館書店 本・書籍ランキングBEST3"
        text2 = "積文館書店 オススメ書籍ランキングをもっと見る"
    elif( category == "cds" ):
        alink = "cds"
        text1 = "積文館書店 CDランキングBEST3"
        text2 = "積文館書店 オススメCDシングルランキングをもっと見る"
    elif( category == "cda" ):
        alink = "cda"
        text1 = "積文館書店 CDランキングBEST3"
        text2 = "積文館書店 オススメCDアルバムランキングをもっと見る"
    elif( category == "dvd" ):
        alink = "dvd"
        text1 = "積文館書店 DVDランキングBEST3"
        text2 = "積文館書店 オススメDVDランキングをもっと見る"
    elif( category == "game" ):
        alink = "game"
        text1 = "積文館書店 ゲームソフトランキングBEST3（NINTENDO DS、Wii、PS2、PS3、PSP等）"
        text2 = "積文館書店 オススメゲームソフトランキングをもっと見る"


    code = ""
    # PRE
    if( category == "cds" ):
        code += '  <div id="#cd_area">\n'
        code += '    <h2><a href="ranking/index.html#' + alink + '"><img src="images/cd_best3.jpg" alt="' + text1 + '" class="mt-15"/></a></h2>\n'
    elif( category != "cda" ):
        code += '  <div id="#' + category + '_area">\n'
        code += '    <h2><a href="ranking/index.html#' + alink + '"><img src="images/' + category + '_best3.jpg" alt="' + text1 + '" class="mt-15"/></a></h2>\n'

    code += '    <div class="best3_area">\n'

    if( category == "cds" ):
        code += '      <img src="images/cd_single.jpg" alt="CDシングルランキング" class="mb-10" />\n'
    elif( category == "cda" ):
        code += '      <img src="images/cd_album.jpg" alt="CDアルバムランキング" class="mb-10" />\n'
    code += '      <ul>\n'

    # MAIN
    cnt = 0
    for list in ranking_list:
        cnt += 1

        if( category != "game" ):
            rank      = list[0]
            title     = list[1]
            author    = list[2]
            honya_url = list[3]
            price     = list[4]
        else:
            rank      = list[0]
            title     = list[1]
            author = hw = list[2]
            maker     = list[3]
            price     = list[4]

        code += '        <li>\n'
        if( category == "cds" or category == "cda" ):
            code += '          <div class="thumb cd">\n'
        else:
            code += '          <div class="thumb ' + category + '">\n'
        # code += '          <div class="thumb ' + category + '">\n'

        if( category != "game" ):
            code += '            <a href="' + honya_url +'" target="_blank">\n'

        if( category == "cds" or category == "cda" ):
            code += '              <p class="ranking_best3 cd">BEST<span>' + str(cnt) + '</span></p>\n'
        else:
            code += '              <p class="ranking_best3 ' + category +'">BEST<span>' + str(cnt) + '</span></p>\n'

        code += '              <img src="ranking/images/' + category + '/0' + str(cnt) + '_org.jpg" alt="">\n'

        if( category != "game" ):
            code += '            </a>\n'
            code += '          </div>\n'
            code += '          <div class="text">\n'
            code += '            <h3><a href="' + honya_url + '" target="_blank">' + title + '</a></h3>\n'
            code += '            <span>' + author + '</span>\n'
            code += '            <span>税込：' + price + '</span>\n'
            code += '          </div>\n'
            code += '        </li>\n'
        else:
            code += '            </a>\n'
            code += '          </div>\n'
            code += '          <div class="text">\n'
            code += '            <h3>' + title + '</h3>\n'
            code += '            <span>ハード：' + hw + '</span>\n'
            code += '            <span>メーカー：' + maker + '</span>\n'
            code += '          </div>\n'
            code += '        </li>\n'

    # POST
    code +=	'      </ul>\n'
    code +=	'      <a class="more_btn" href="ranking/index.html#' + alink + '">\n'
    code +=	'        <img src="images/ranking_bt.jpg" alt="' + text2 + '"/>\n'
    code +=	'      </a>\n'
    code +=	'    </div>\n'

    if( category != "cds" ):
        code +=	'  </div>\n'
    code +=	'\n'

    # print( code )
    return code


# ---- Main
if __name__ == '__main__':
    args = sys.argv

    if( 2 < len(args) ):
        IN_CSV_1 = args[1]
        IN_CSV_2 = args[2]
    else:
        IN_CSV_1 = "tsutaya_ranking_update.csv"
        IN_CSV_2 = "sekibunkan_ranking_update.csv"

    OUT_HTML = "dist/top3.html"

    tsutaya_csv    = []
    sekibunkan_csv = []

    # Read old list
    with open(IN_CSV_1, encoding="utf_8_sig") as f:
        reader = csv.reader(f)
        for i in reader:
            tsutaya_csv.append( i )

    with open(IN_CSV_2, encoding="utf_8_sig") as f:
        reader = csv.reader(f)
        for i in reader:
            sekibunkan_csv.append( i )

    print( ">>> Generate top3 HTML code ...")
    gen_html( tsutaya_csv, sekibunkan_csv, OUT_HTML )
    print( ">>> Generate top3 HTML code --> DONE")
