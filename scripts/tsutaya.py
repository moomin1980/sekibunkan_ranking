# coding:utf-8

import os
import sys
import urllib3
import csv
import re
import mojimoji
import glob
import shutil
import datetime
import time
import glob
from PIL import Image
from bs4 import BeautifulSoup
from img_dl import img_dl
from gen_html import gen_html

# Site setting
SITE_URL = []
SITE_URL.append( "https://store-tsutaya.tsite.jp/rank/cd_sell.html?r=W033" )
SITE_URL.append( "https://store-tsutaya.tsite.jp/rank/cd_sell.html?s=3" )
SITE_URL.append( "https://store-tsutaya.tsite.jp/rank/dvd_sell.html?s=3" )
SITE_URL.append( "https://store-tsutaya.tsite.jp/rank/game.html?r=W090&moid=rank_sgame" )
ROOT_URL = "https://store-tsutaya.tsite.jp"

HONYA = "https://www.honyaclub.com/shop/goods/search.aspx?cat_p=&search=x&keyw="
HONYA_ROOT = "https://www.honyaclub.com"

CSV_FILE = "tsutaya_ranking.csv"        # CSV file name (output)
OUT_FILE = "tsutaya_ranking_update.csv" # CSV file name (output)
DIST_DIR = "dist/ranking/images/"       # Distribution directory
HTML_FILE = "dist/ranking/index.html"   # HTML file name (output)
TEMPLATE_FILE = "template/index.html"   # HTML template file
BOOK_TEMPLATE_FILE = "template/books.html"   # HTML template file
NO_IMAGE_FILE = "dist/ranking/assets/noimage.jpg"       # No Image

BACK_DIR = "backup/"                    # Backup ranking data folder
DISP_NUM = 10               # Number of display ranking
TEXT_HALF = True            # True : Change text to halfwidth
CD_IMG_WIDTH = 140          # Output image width for CD & DVD
GAME_IMG_WIDTH = 160        # Output image width for GAME
# ----------
def GetCategoryRanking( category, url ):
    # Get HTML data
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')

    # Ranking list
    list = []

    ranking_list = soup.select( ".groupBlock" )

    # 以下の方式では DVD でエラーになるケースがあったためコメントアウト
    # sec = soup.select_one('#styleChangeViewTile')
    # ranking_list = sec[0].find_all('div', class_='groupBlock')
    # ranking_list = sec.select("div .groupBlock")

    if( DISP_NUM <= len( ranking_list ) ):
        for i in range( DISP_NUM ):
            item = ranking_list[i]

            title  = ( item.find('div', class_='groupImage').img.get("title") ).strip()
            #  CDアルバム　オムニバスの時、アーティスト名は空になるので注意  (ルール化が難しい場所)
            author_base = ( item.find('div', class_='groupText') ).select('p')
            if( 1 < len(author_base) ):
                author = author_base[1].get_text().strip()
            else:
                author = ""         # 不明な場合は空欄
            try:
                href = ( item.find('div', class_='groupImage').a.get("href") ).strip()
            except:
                print( "[ERROR] : Can't get TSUTAYA URL. | Titile = " + str( title ) )
                href = ""

            img    = ROOT_URL + ( item.find('div', class_='groupImage') ).img.get("src").strip()

            # Get more information
            if( href ):
                img_large, jan, maker, artist = GetItemDetail( href )
            else:
                img_large = ""
                jan = ""
                maker = ""
                artist = ""

            if( category == "dvd"):
                # Overwrite author by artist
                author = artist.strip()

            if( TEXT_HALF ):
                title     = mojimoji.zen_to_han( title, kana=False )
                author    = mojimoji.zen_to_han( author, kana=False )
                href      = mojimoji.zen_to_han( href, kana=False )
                img       = mojimoji.zen_to_han( img, kana=False )
                img_large = mojimoji.zen_to_han( img_large, kana=False )
                jan       = mojimoji.zen_to_han( jan, kana=False )
                maker     = mojimoji.zen_to_han( maker, kana=False )

            print( "    [" + category + "] No." + str(i + 1) + " " + title )
            # print( item ) # print( "  " + author ) # print( "  " + href ) # print( "  " + img )
            data = [ category, i + 1, title, author, href, img, img_large, jan, maker ]
            list.append( data )
            #
            # dir = DIST_DIR + category + "/"
            # filename = dir + '{0:02d}'.format(i + 1) + ".jpg"
            # img_dl( img_large, filename )

    else:
        print( "[ERROR] : Can't get item list" )

    return list

# ----------
def GetItemDetail( url ):
    # Get HTML data
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    # print( soup )

    img_url = ROOT_URL + ( soup.select('.productBox') )[0].img.get("src").strip()
    print( img_url )

    info = ( soup.select('.detailBox ul') )[1]
    list = info.select('li')

    jan = ""
    maker = ""
    artist = ""
    for li in list:
        text = ( li.get_text() ).split('：')
        if( 2 <= len(text) ):
            # print( text )
            label = text[0]
            data = text[1]
            if( label == "JAN" ):
                jan = ( re.sub(r'\D', '', data) ).strip()
            if( label == "販売元" ):
                maker = data.strip()
            if( label == "出演者" or label == "原作者" ):
                artist = data.strip()
            elif( label == "監督" ):
                artist = "xx_movie_xx"

    return img_url, jan, maker, artist

# ----------
def ResizeImage( dir, category ):
    files = glob.glob( dir + category + "/*_org.jpg")
    # print( files )
    for file in files:
        root, ext = os.path.splitext( file )
        outfile = root.replace('_org', '') + ext
        print( "    " + outfile )

        # Load original file
        im = Image.open( file )
        w, h = im.size
        if( category == "game" ):
            re_w = GAME_IMG_WIDTH
            re_h = int( GAME_IMG_WIDTH * h / w )
        else:
            re_w = CD_IMG_WIDTH
            re_h = int( CD_IMG_WIDTH * h / w )

        img_resize = im.resize( ( re_w, re_h ), Image.BICUBIC )
        img_resize.save( outfile, quality=95 )

# ----------
def GetHonyaClubUrl( jan ):
    # Get HTML data
    http = urllib3.PoolManager()
    url = HONYA + jan
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    sec  = soup.find('div', class_='result-item-inner')

    try:
        item_url = HONYA_ROOT + ( sec.select('.item-img') )[0].a.get("href").strip()
        print( "    " + item_url )
    except:
        print( "[ERROR] : Can't get Honya Club URL. | JAN = " + str( jan ) )
        item_url = ""

    return item_url

# ----------
def GetHonyaClubActor( url ):
    # Get HTML data
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    sec  = soup.find('div', class_='detail-item')
    try:
        actor = ( sec.select('.item-info a') )[2].get_text().strip()
        # print( actor )
    except:
        print( "[ERROR] : Can't get Honya Club actor information. | URL = " + str(url) )
        actor = ""

    return actor

# ----------
def GetHonyaClubPrice( url ):
    # Get HTML data
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    sec  = soup.find('div', class_='detail-item')
    try:
        price = ( sec.select('.item-price dd') )[0].get_text().strip()
        price = price.split( '（' )[0]
    except:
        print( "[ERROR] : Can't get Honya Club price information. | URL = " + str(url) )
        price = "--"

    # print( actor )
    return price


# ---------------
#  Main
# ---------------
if __name__ == '__main__':
    args = sys.argv

    if( 1 < len(args) ):
        print( "\n[ Single mode selected ]\n" )
        option = int( args[1] )
        single = True
        timer  = False
    else:
        single = False
        timer  = True

    # Set start time
    if( timer ):
        t1 = time.time()

    # Get tsutaya ranking data
    if( (single == False) or (option == 1) ):
        print( ">>> Get TSUTAYA ranking data ...")
        ranking = []
        ranking.extend( GetCategoryRanking( "cds"  ,SITE_URL[0] ) )
        ranking.extend( GetCategoryRanking( "cda"  ,SITE_URL[1] ) )
        ranking.extend( GetCategoryRanking( "dvd"  ,SITE_URL[2] ) )
        ranking.extend( GetCategoryRanking( "game" ,SITE_URL[3] ) )
        print( ">>> Get TSUTAYA ranking data --> DONE")


        # Save csv file
        print( ">>> Save ranking data ...")
        with open(CSV_FILE, 'w', encoding="utf_8_sig") as f:
            writer = csv.writer(f, lineterminator='\n')
            for i in ranking:
                writer.writerow( i )
        print( ">>> Save ranking data --> DONE")

    # Download jacket image files
    if( (single == False) or (option == 2) ):
        print( ">>> Download jacket image file ...")

        with open(CSV_FILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            for row in reader:
                category = row[0]
                ranking = int( row[1] )
                img_url = row[6]
                # Save image file
                dir = DIST_DIR + category + "/"
                filename = dir + '{0:02d}'.format(ranking) + "_org.jpg"
                if( img_url ):
                    img_dl( img_url, filename )
                else:
                    shutil.copy( NO_IMAGE_FILE, filename )
                    print( "    [Skip]   -> " + filename )

        print( ">>> Download jacket image file --> DONE")

    # Download jacket image files
    if( (single == False) or (option == 3) ):
        print( ">>> Resize image file ...")
        ResizeImage( DIST_DIR, "cds" )
        ResizeImage( DIST_DIR, "cda" )
        ResizeImage( DIST_DIR, "dvd" )
        ResizeImage( DIST_DIR, "game" )
        print( ">>> Resize image file --> DONE")

    # Get "Honya Club" information from JAN CODE
    if( (single == False) or (option == 4) ):
        update = []
        print( ">>> Get Honya Club information ...")
        with open(CSV_FILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            for row in reader:
                # Add Honya Club url from JAN code
                cat    = row[0]
                author = hw = row[3]
                jan    = row[7]
                if( cat != "game" ):
                    url = GetHonyaClubUrl( jan )
                    row.append( url )
                    # Replace actor information
                    if( (cat == "dvd") and (author == "xx_movie_xx") ):
                        row[3] = GetHonyaClubActor( url )
                else:
                    # Replace text
                    if( hw == "プレイステーション4" ):
                        row[3] = "PS4"
                    row.append( "--" )    # Url is none for game catergoy
                # Add price
                if( (cat != "game") and (url != "") ):
                    price = GetHonyaClubPrice( url )
                else:
                    price = "--"
                row.append( price )

                # Copy list
                update.append( row )

        print( ">>> Get Honya Club information --> DONE")

        # Save csv file
        print( ">>> Save updated ranking data ...")
        with open(OUT_FILE, 'w', encoding="utf_8_sig") as f:
            writer = csv.writer(f, lineterminator='\n')
            for i in update:
                writer.writerow( i )
        print( ">>> Save updated ranking data --> DONE")

    # Check tsutaya ranking transition and update CSV file
    if( (single == False) or (option == 5) ):
        print( ">>> Check ranking transition ...")
        OLD_FILES = glob.glob('backup/TSUTAYA_*')

        old_list = []
        new_list = []

        if( OLD_FILES ):
            OLD_FILE = max(OLD_FILES, key=os.path.getctime)
            print( OLD_FILE )

            # Read old list
            with open(OLD_FILE, encoding="utf_8_sig") as f:
                reader = csv.reader(f)
                for i in reader:
                    old_list.append( i )
            # Read new list
            with open(OUT_FILE, encoding="utf_8_sig") as f:
                reader = csv.reader(f)
                for i in reader:
                    new_list.append( i )

            # Search old data
            for i in new_list:
                category = i[0]
                title    = i[2]
                jan      = i[7]

                trans = "new"
                for j in old_list:
                    if( jan in j ):
                        # Hit old ranking data
                        old_rank = int( j[1] )
                        new_rank = int( i[1] )
                        # print( "old_rank " + str( old_rank ) )
                        # print( "new_rank " + str( new_rank ) )

                        if( new_rank < old_rank ):
                            trans = "up"
                        elif( old_rank < new_rank ):
                            trans = "down"
                        else:
                            trans = "stay"

                        # print( "trans " + str( trans ) )
                # print( "[ " + category + " ] [ " + trans + " ] " + title  )
                i.append( trans )
        else:
            print( ">>> !! Can not find old ranking data !!")
        print( ">>> Check ranking transition --> DONE")

        # Save csv file
        print( ">>> Save updated ranking data ...")
        with open(OUT_FILE, 'w', encoding="utf_8_sig") as f:
            writer = csv.writer(f, lineterminator='\n')
            for i in new_list:
                writer.writerow( i )
        print( ">>> Save updated ranking data --> DONE")

    # Generate HTML code
    if( (single == False) or (option == 6) ):
        print( ">>> Generate HTML code ...")
        with open(OUT_FILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            gen_html( reader, HTML_FILE, TEMPLATE_FILE, BOOK_TEMPLATE_FILE )

        print( ">>> Generate HTML code --> DONE")

    # Backup tsutaya ranking data for next time
    if( (single == False) or (option == 7) ):
        print( ">>> Backup TSUTAYA ranking data ...")
        dt_today = datetime.date.today()
        today    = dt_today.strftime('%Y%m%d')
        print( "    Today : " + today )
        if( not os.path.isdir( BACK_DIR ) ):
            os.makedirs( BACK_DIR )
        outfile = BACK_DIR + "TSUTAYA_" + today + ".csv"
        shutil.copy( OUT_FILE, outfile )
        print( ">>> Backup TSUTAYA ranking data --> DONE")

    # Set end time
    if( timer ):
        t2 = time.time()
        elapse = t2 - t1
        print( "TIME = ", elapse )
