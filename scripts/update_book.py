# coding:utf-8

import os
import sys
import urllib3
import csv
import re
import mojimoji
import glob
import shutil
import datetime
import time
from PIL import Image
from bs4 import BeautifulSoup
from img_dl import img_dl
from gen_html import book_part

dt_now   = datetime.datetime.now()
year  = str( dt_now.year )
month = str( '{0:02d}'.format( dt_now.month ) )
day   = str( '{0:02d}'.format( dt_now.day ) )

ROOT_URL = "https://store-tsutaya.tsite.jp"
SEARCH_URL_PRE = "https://store-tsutaya.tsite.jp/item/search_s/search_s_result.html?gn=&pn=&cpn=&isbn="
SEARCH_URL_POST = "&lj=&mj=&dfy=&dfm=&dfd=&dty=&dtm=&dtd=&st=&i=130&ctp=130&stp=1"

HONYA = "https://www.honyaclub.com/shop/goods/search.aspx?cat_p=&search=x&keyw="
HONYA_ROOT = "https://www.honyaclub.com"

CSV_FILE = "sekibunkan_ranking.csv"        # CSV file name (output)
OUT_FILE = "sekibunkan_ranking_update.csv" # CSV file name (output)
DIST_DIR = "dist/ranking/images/"          # Distribution directory

# HTML_FILE = "dist/ranking/index.html"   # HTML file name (output)
TEMPLATE_FILE = "template/books.html"   # HTML template file

BACK_DIR = "backup/"                    # Backup ranking data folder
DISP_NUM = 10               # Number of display ranking
TEXT_HALF = True            # True : Change text to halfwidth
CD_IMG_WIDTH = 140          # Output image width for CD & DVD
GAME_IMG_WIDTH = 160        # Output image width for GAME
BOOK_IMG_WIDTH = 140        # Output image width for BOOK
# ----------

# ----------
def ResizeImage( dir, category ):
    files = glob.glob( dir + category + "/*_org.jpg")
    # print( files )
    for file in files:
        root, ext = os.path.splitext( file )
        outfile = root.replace('_org', '') + ext
        print( "    " + outfile )

        # Load original file
        im = Image.open( file )
        w, h = im.size
        if( category == "game" ):
            re_w = GAME_IMG_WIDTH
            re_h = int( GAME_IMG_WIDTH * h / w )
        elif( category == "book" ):
            re_w = BOOK_IMG_WIDTH
            re_h = int( BOOK_IMG_WIDTH * h / w )
        else:
            re_w = CD_IMG_WIDTH
            re_h = int( CD_IMG_WIDTH * h / w )

        img_resize = im.resize( ( re_w, re_h ), Image.BICUBIC )
        img_resize.save( outfile, quality=95 )


# ----------
def GetHonyaClubUrl( jan ):
    # Get HTML data
    http = urllib3.PoolManager()
    url = HONYA + jan
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    sec  = soup.find('div', class_='result-item-inner')
    item_url = HONYA_ROOT + ( sec.select('.item-img') )[0].a.get("href").strip()
    print( "    " + item_url )
    return item_url


def GetTsutayaImageFileUrl( isbn ):
    # Get HTML data
    http = urllib3.PoolManager()
    url = SEARCH_URL_PRE + str( isbn ) + SEARCH_URL_POST
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    item_url1  = ROOT_URL + ( soup.select('.itemDetail .imgBox')[0] ).a.get("href").strip()

    r = http.request('GET', item_url1)
    soup = BeautifulSoup(r.data, 'html.parser')
    item_url2  = ROOT_URL + ( soup.select('.imgBox .productBox')[0] ).img.get("src").strip()

    return item_url2

# ----------
def GetHonyaClubDetail( url ):
    # Get HTML data
    http = urllib3.PoolManager()
    r = http.request('GET', url)
    soup = BeautifulSoup(r.data, 'html.parser')
    sec  = soup.find('div', class_='detail-item')
    # print( sec )

    # Get image url
    image_url = HONYA_ROOT + ( sec.select('.item-img img') )[0].get("src").strip()
    title = ( sec.select('.item-txt dt') )[0].get_text().strip()

    text_box = ( sec.select('.item-txt .item-sub a') )
    publisher =  text_box[0].get_text().strip()
    if( 1 < len(text_box) ):
        author = text_box[1].get_text().strip()
    else:
        author = ""
    price = ( sec.select('.item-txt .item-price dd') )[0].get_text().strip()
    price = ( price.split( "（" ) )[0]
    # print( title + " " + publisher + " " + author + " " + price )

    if( TEXT_HALF ):
        title     = mojimoji.zen_to_han( title, kana=False )
        author    = mojimoji.zen_to_han( author, kana=False )
        publisher = mojimoji.zen_to_han( publisher, kana=False )
        price     = mojimoji.zen_to_han( price, kana=False )
        image_url = mojimoji.zen_to_han( image_url, kana=False )

    return title, author, publisher, price, image_url


# ---- Function
def update_html( csv, template ):

    book = []
    for row in csv:
        category = row[0]
        rank = row[1]
        title = row[2]
        author = row[3]
        publisher = row[4]
        # price = row[5]
        # isbn = row[6]
        honya_url = row[7]
        trans = row[9]

        book.append( [rank, title, author, publisher, honya_url, trans] )
        # print( game )

    # Read template file
    # with open(template, encoding="utf_8_sig") as f:
    #     reader = f.readlines()

    # Generate HTML code
    f = open( template, 'w', encoding="utf_8_sig")
    # Write html header
    # f.write( header )

    # Book part
    code = book_part( book )
    f.write( code )

    # Wrtie html footer
    # f.write( footer )
    f.close()





# ---------------
#  Main
# ---------------
if __name__ == '__main__':
    args = sys.argv

    SEKIBUNKAN_CSV = args[1]
    category = "book"

    if( 2 < len(args) ):
        print( "\n[ Single mode selected ]\n" )
        option = int( args[2] )
        single = True
        timer  = False
    else:
        single = False
        timer  = True

    # Set start time
    if( timer ):
        t1 = time.time()

    # Get "Honya Club" information from ISBN CODE
    if( (single == False) or (option == 1) ):
        list = []

        print( ">>> Get Honya Club information ...")
        with open(SEKIBUNKAN_CSV, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            cnt = 0
            for row in reader:
                if( (2 < cnt) and (cnt < 13) ):
                    ranking = int(row[0])
                    isbn = row[5]
                    if( ranking <= DISP_NUM ):
                        url = GetHonyaClubUrl( isbn )
                        # Copy list
                        title, author, publisher, price, image_url = GetHonyaClubDetail( url )
                        li = [ category, ranking, title, author, publisher, price, int(isbn), url, image_url ]
                        list.append( li )
                cnt += 1
        print( ">>> Get Honya Club information --> DONE")

        # Save csv file
        print( ">>> Save sekibunkan ranking data ...")
        with open(CSV_FILE, 'w', encoding="utf_8_sig") as f:
            writer = csv.writer(f, lineterminator='\n')
            for i in list:
                writer.writerow( i )
        print( ">>> Save sekibunkan ranking data --> DONE")



    # Download jacket image files
    if( (single == False) or (option == 2) ):
        print( ">>> Download jacket image file ...")

        with open(CSV_FILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            for row in reader:
                category = "book"
                ranking = int( row[1] )
                isbn = int( row[6] )
                img_url = GetTsutayaImageFileUrl( isbn )

                # Save image file
                dir = DIST_DIR + category + "/"
                filename = dir + '{0:02d}'.format(ranking) + "_org.jpg"
                img_dl( img_url, filename )

        print( ">>> Download jacket image file --> DONE")


    # Resize jacket image files
    if( (single == False) or (option == 3) ):
        print( ">>> Resize image file ...")
        ResizeImage( DIST_DIR, "book" )
        print( ">>> Resize image file --> DONE")

    # Update ranking transition
    if( (single == False) or (option == 4) ):
        print( ">>> Update ranking data ...")

        OLD_FILES = glob.glob('backup/SEKIBUNKAN_*')

        old_list = []
        new_list = []

        if( OLD_FILES ):
            OLD_FILE = max(OLD_FILES, key=os.path.getctime)
            print( OLD_FILE )

            # Read old list
            with open(OLD_FILE, encoding="utf_8_sig") as f:
                reader = csv.reader(f)
                for i in reader:
                    old_list.append( i )
            # Read new list
            with open(CSV_FILE, encoding="utf_8_sig") as f:
                reader = csv.reader(f)
                for i in reader:
                    new_list.append( i )

            # Search old data
            for i in new_list:
                category = i[0]
                title    = i[2]
                jan      = i[6]

                trans = "new"
                for j in old_list:
                    if( jan in j ):
                        # Hit old ranking data
                        old_rank = int( j[1] )
                        new_rank = int( i[1] )
                        if( new_rank < old_rank ):
                            trans = "up"
                        elif( old_rank < new_rank ):
                            trans = "down"
                        else:
                            trans = "stay"
                # print( "[ " + category + " ] [ " + trans + " ] " + title  )
                i.append( trans )
        else:
            print( ">>> !! Can not find old ranking data !!")

        print( ">>> Update ranking data --> DONE")

        # Save csv file
        print( ">>> Save updated ranking data ...")
        with open(OUT_FILE, 'w', encoding="utf_8_sig") as f:
            writer = csv.writer(f, lineterminator='\n')
            for i in new_list:
                writer.writerow( i )
        print( ">>> Save updated ranking data --> DONE")

    # Generate HTML code
    if( (single == False) or (option == 5) ):
        print( ">>> Generate HTML code ...")
        with open(OUT_FILE, encoding="utf_8_sig") as f:
            reader = csv.reader(f)
            update_html( reader, TEMPLATE_FILE )

        print( ">>> Generate HTML code --> DONE")

    # Backup tsutaya ranking data for next time
    if( (single == False) or (option == 6) ):
        print( ">>> Backup SEKIBUNKAN ranking data ...")
        dt_today = datetime.date.today()
        today    = dt_today.strftime('%Y%m%d')
        print( "    Today : " + today )
        if( not os.path.isdir( BACK_DIR ) ):
            os.makedirs( BACK_DIR )
        outfile = BACK_DIR + "SEKIBUNKAN_" + today + ".csv"
        shutil.copy( OUT_FILE, outfile )
        print( ">>> Backup SEKIBUNKAN ranking data --> DONE")

    # Set end time
    if( timer ):
        t2 = time.time()
        elapse = t2 - t1
        print( "TIME = ", elapse )
